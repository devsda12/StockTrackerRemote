import * as SQLite from 'expo-sqlite';
import { generateID } from './UserDBHelper';
import * as FileSystem from 'expo-file-system';

let db = null;

export function openDatabase(uid){
  if (Platform.OS === "web") {
      return {
        transaction: () => {
          return {
            executeSql: () => {},
          };
        },
      };
  }

  db = SQLite.openDatabase("STR-" + uid + ".db");
  
  //Creating the table if not yet exists
  db.transaction((tx) => {
    tx.executeSql("CREATE TABLE IF NOT EXISTS items (id INTEGER PRIMARY KEY, name TEXT NOT NULL, description TEXT, category TEXT, condition TEXT NOT NULL, visual BLOB);");
  });
}

//#region RETRIEVAL
export function retrieveAll(){
    return new Promise((resolve, reject) => {
        db.transaction((tx) => {
            tx.executeSql("SELECT * FROM items", [], (_, result) => resolve(result), (_, error) => reject(error));
        });
    });
}

export function retrieveAllByName(name){
    let pattern = "%" + name + "%"

    return new Promise((resolve, reject) => {
        db.transaction((tx) => {
            tx.executeSql("SELECT * FROM items WHERE name LIKE ?", [pattern], (_, result) => resolve(result), (_, error) => reject(error));
        });
    });
}

export function retrieveSmallAll(){
    return new Promise((resolve, reject) => {
        db.transaction((tx) => {
            tx.executeSql("SELECT id, name, description, category, visual FROM items", [], (_, result) => resolve(result), (_, error) => reject(error));
        });
    });
}

export function retrieveSmallByName(name){
    let pattern = "%" + name + "%"

    return new Promise((resolve, reject) => {
        db.transaction((tx) => {
            tx.executeSql("SELECT id, name, description, category, visual FROM items WHERE name LIKE ?", [pattern], (_, result) => resolve(result), (_, error) => reject(error));
        });
    });
}
//#endregion

//#region MODIFICATION
export async function insertItem(name, description, category, condition, visual){
    //Creating a new UID
    let uid = await createUID();

    //Updating visual path
    //visual = constructVisualPath(uid);

    //Inserting into the table
    return new Promise((resolve, reject) => {
        db.transaction((tx) => {
            tx.executeSql("INSERT INTO items (id, name, description, category, condition, visual) values (?, ?, ?, ?, ?, ?)", [uid, name, description, category, condition, visual],
            (_, result) => resolve({uid, result}), (_, error) => reject(error));
        });
    });
}

export function updateItem(id, updatedObj){
    //Getting the necessary sql string parts
    // let propNames = "";
    // Object.keys(updatedObj).forEach((key) => {
    //     propNames += key + " = '" + updatedObj[key] + "', ";
    // });
    // propNames = propNames.slice(0, -2);

    //updatedObj.visual = constructVisualPath(id);

    //Updating in the table
    return new Promise((resolve, reject) => {
        // db.transaction((tx) =>{ //UNFORTUNATELY COULDN'T USE THIS AS THE SQLITE PLUGIN PRE-COMPILES THE QUERY BEFORE ADDING THE VARIABLES REPLACING THE QUESTION MARKS
        //     tx.executeSql("UPDATE items SET ? WHERE id = ?", [propNames, id], (_, result) => resolve(result), (_, error) => reject(error));
        // });
        db.transaction((tx) => {
            tx.executeSql("UPDATE items SET name = ?, description = ?, category = ?, condition = ?, visual = ? WHERE id = ?", [updatedObj.name, updatedObj.description, updatedObj.category, updatedObj.condition, updatedObj.visual, id], (_, result) => resolve(result), (_, error) => reject(error));
        });
    });
}

export function deleteItem(id){
    //Deleting from the table
    return new Promise((resolve, reject) => {
        db.transaction((tx) => {
            tx.executeSql("DELETE FROM items WHERE id = ?", [id], (_, result) => resolve(result), (_, error) => reject(error));
        });
    });
}
//#endregion

//#region Helper functions
function constructVisualPath(itemID){
    let dirPath = FileSystem.documentDirectory + "images-" + global.currUID + "/";
    let filePath = dirPath + itemID + ".jpg";
    return filePath;
}

async function createUID(){
    //Generating a number
    let number = generateID();
    let exists = () => { 
        return new Promise((resolve, reject) => {
            db.transaction((tx) => {
                tx.executeSql("SELECT id FROM items WHERE id = ?", [number], (_, result) => resolve(result.rows.length > 0), (_, error) => reject(error));
            });
        });
    }

    while(await exists()) number = generateID();
    return number;
}
//#endregion