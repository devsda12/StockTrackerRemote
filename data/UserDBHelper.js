import * as SQLite from 'expo-sqlite';

const db = openDatabase();

function openDatabase(){
  if (Platform.OS === "web") {
      return {
        transaction: () => {
          return {
            executeSql: () => {},
          };
        },
      };
  }

  let db = SQLite.openDatabase("STRU.db", "0.2");
  
  //Creating the table if not yet exists
  db.transaction((tx) => {
    tx.executeSql("CREATE TABLE IF NOT EXISTS users (uid INTEGER PRIMARY KEY, uname TEXT NOT NULL, passwd TEXT NOT NULL);");
  });

  return db;
}

//#region RETRIEVAL
export function unameExists(uname){
  return new Promise((resolve, reject) => {
    //Checking if the uname is yet been taken
    db.transaction((tx) =>{
      tx.executeSql("SELECT uname FROM users WHERE uname = ?", [uname], (_, result) => resolve(result.rows.length > 0), (_, error) => reject(error));
    });
  })
}

export async function checkUnamePasswd(uname, passwd){ //For now just using the password as plain text, this is not desired
  return new Promise((resolve, reject) => {
    //Checking if the two match
    db.transaction((tx) => {
      tx.executeSql("SELECT uid, passwd FROM users WHERE uname = ?", [uname], (_, result) => {
        if(result.rows.length > 0) resolve({matches: result.rows.item(0)["passwd"] === passwd, uid: result.rows.item(0)["uid"]});
        else resolve({matches: false});
      }, (_, error) => reject(error));
    });
  })
}
//#endregion

//#region MODIFICATION
export async function insertUser(uname, passwd){ //For now just using the password as plain text, this is not desired
  //Creating a new UID
  let uid = await createUID();

  //Inserting into the table
  db.transaction((tx) => {
    tx.executeSql("INSERT INTO users (uid, uname, passwd) values (?, ?, ?)", [uid, uname, passwd], (_,__) => console.log("succes"), (_, error) => console.log(error));
  });
}
//#endregion

//#region Helper functions
async function createUID(){
  //Generating a number
  let number = generateID();
  let exists = () => { 
    return new Promise((resolve, reject) => {
      db.transaction((tx) => {
        tx.executeSql("SELECT uid FROM users WHERE uid = ?", [number], (_, result) => resolve(result.rows.length > 0), (_, error) => reject(error));
      });
    });
  }

  while(await exists()) number = generateID();
  return number;
}

export function generateID() { return Math.round(Math.random() * (99999999 - 10000000) + 10000000); }
//#endregion