export const dummyData = [
    {
        "id" : "abcdefg", //Determined by the barcode if present, otherwise a random number which does not exist yet
        "name" : "LCD Panel", //Could be anything
        "description" : "", //Could be anything
        "category" : "Screens", //Could be anything the user defines, need to add to table when a new one is presented (maybe tree structure?)
        "condition" : "operable", //Could be 'unknown' / 'broken' / 'defects' / 'partial' / 'operable' / 'as new'
        "visual" : null //Blob of image
    },
    {
        "id" : "hijklmnop", //Determined by the barcode if present, otherwise a random number which does not exist yet
        "name" : "LED Panel", //Could be anything
        "description" : "", //Could be anything
        "category" : "Screens", //Could be anything the user defines, need to add to table when a new one is presented (maybe tree structure?)
        "condition" : "unknown", //Could be 'unknown' / 'broken' / 'defects' / 'partial' / 'operable' / 'as new'
        "visual" : null //Blob of image
    }
]