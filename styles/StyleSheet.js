import { StyleSheet } from 'react-native';

export const colors = {
    "color_dark1_10" : "#0a0612",
    "color_dark2_10" : "#150c25",
    "color_dark3_10" : "#1f1137",
    "color_dark4_10" : "#291749",
    "color_dark5_10" : "#341d5c",
    "color_dark6_10" : "#3e236e",
    "color_dark7_10" : "#482980",
    "color_dark8_10" : "#522e92",
    "color_dark9_10" : "#5d34a5",
    "color_dark10_10" : "#673ab7",
}

export const textColors = {
    "disabledText" : "#d3d3d3",
    "minTransWhite" : "rgba(255, 255, 255, 0.7)",
    "maxTransWhite" : "rgba(255, 255, 255, 0.3)"
}

export const borderColors = {
    "lightGrey" : "#6f6f6f"
}

export const globalStyles = StyleSheet.create({
    dark1_10:{
        backgroundColor: colors["color_dark1_10"]
    },
    dark2_10:{
        backgroundColor: colors["color_dark2_10"]
    },
    dark3_10:{
        backgroundColor: colors["color_dark3_10"]
    },
    dark4_10:{
        backgroundColor: colors["color_dark4_10"]
    },
    dark5_10:{
        backgroundColor: colors["color_dark5_10"],
        color: colors["color_dark5_10"]
    },
    dark6_10:{
        backgroundColor: colors["color_dark6_10"],
        color: colors["color_dark6_10"]
    },
    dark7_10:{
        backgroundColor: colors["color_dark7_10"],
        color: colors["color_dark7_10"]
    },
    dark8_10:{
        backgroundColor: colors["color_dark8_10"],
        color: colors["color_dark8_10"]
    },
    dark9_10:{
        backgroundColor: colors["color_dark9_10"],
        color: colors["color_dark9_10"]
    },
    dark10_10:{
        backgroundColor: colors["color_dark10_10"],
        color: colors["color_dark10_10"]
    }
});