import React, {Component} from 'react';
import { StyleSheet, Button, Text, View, ImageBackground, TextInput, Dimensions, TouchableOpacity, Image, Alert, Platform, ToastAndroid} from 'react-native';
import { ActivityIndicator } from 'react-native-paper';
import { openDatabase } from '../data/ContentDBHelper';
import { checkUnamePasswd, unameExists } from '../data/UserDBHelper';
import { globalStyles, colors, textColors } from '../styles/StyleSheet';

//#region Consts
const screenWidth = Dimensions.get("window").width;
//#endregion

export class LandingScreen extends Component{
    state = {
        uname: "",
        passwd: "",
        loggingIn: false
    }

    login = function() {
        //Checking if uname / password is not null
        if(this.state.uname === "" || this.state.passwd === ""){
            Platform.OS === "android" ? ToastAndroid.show("No username / password specified!", ToastAndroid.SHORT) : Alert.alert("No username / password specified!");
            return;
        }

        this.setState({loggingIn: true});

        checkUnamePasswd(this.state.uname, this.state.passwd)
        .then((result) => {
            this.setState({loggingIn: false});
            if(result.matches){
                //Clearing uname and passwd
                // this.setState({uname: "", passwd: ""});

                //Saving uid to global variable and opening the appropriate db with it
                global.currUID = result.uid;
                openDatabase(global.currUID);

                this.props.navigation.navigate("AppContent");
            }else{
                Platform.OS === "android" ? ToastAndroid.show("Username / Password combination is incorrect", ToastAndroid.SHORT) : Alert.alert("Username / Password combination is incorrect");
            }
        })
        .catch((error) => console.log(error))
    }

    render(){
        return(
            <ImageBackground source={require("../assets/landingScreenBackground.png")} style={landingScreenStyles.imageBackground}>
                <View style={landingScreenStyles.loginContainer}>
                    <Image style={landingScreenStyles.textLogo} source={require("../assets/logo_text.png")}/>
                    <LoginTextField placeholder="Username" secureTextEntry={false} onValueChange={(value) => this.setState({uname: value}) }/>
                    <LoginTextField placeholder="Password" secureTextEntry={true} onValueChange={(value) => this.setState({passwd: value}) }/>
                    
                    {!this.state.loggingIn && 
                        <TouchableOpacity style={landingScreenStyles.loginButton} onPress={() => this.login()}>
                            <Text style={landingScreenStyles.text}>Login</Text>
                        </TouchableOpacity>
                    }
                    {this.state.loggingIn &&
                        <View style={landingScreenStyles.loginActivityIndicatorContainer}><ActivityIndicator animating={true} /></View>
                    }
                    
                    <TouchableOpacity style={[landingScreenStyles.loginButton, {marginTop: 15}]} onPress={() => this.props.navigation.navigate("NewUserScreen")}>
                        <Text style={landingScreenStyles.text}>Create Account</Text>
                    </TouchableOpacity>
                    <Text style={{color: textColors["minTransWhite"], fontSize: 12, marginTop: 15}}>*Only local databases supported at the moment</Text>
                </View>
            </ImageBackground>
        );
    }
}

export class LoginTextField extends Component{
    render(){
        return(
            <TextInput 
            style={landingScreenStyles.loginTextField}
            onChangeText={(text) => this.props.onValueChange(text)}
            placeholder={this.props.placeholder}
            secureTextEntry={this.props.secureTextEntry}
            placeholderTextColor={textColors["maxTransWhite"]}
            underlineColorAndroid="transparent"/>
        );
    }
}

export class TestScreen extends Component{
    render(){
        return(
            <View>
                <Text>
                    Hello there too mate!!
                </Text>
            </View>
        );
    }
}

export const landingScreenStyles = StyleSheet.create({
    imageBackground: {
        width: '100%',
        height: '100%'
    },
    loginContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textLogo:{
        width: "60%",
        height: "30%",
        resizeMode: 'center',
        marginTop: "-30%"
    },
    loginTextField:{
        width: screenWidth - 55,
        height: 45,
        paddingHorizontal: 10,
        marginBottom: 15,
        borderRadius: 25,
        backgroundColor: colors["color_dark9_10"],
        fontSize: 16,
        color: textColors["minTransWhite"]
    },
    loginButton: {
        width: screenWidth - 55,
        height: 60,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 25,
        backgroundColor: colors["color_dark6_10"],
    },
    loginActivityIndicatorContainer: {
        width: screenWidth - 55,
        height: 60,
        alignItems: 'center',
        justifyContent: 'center',
    },
    text:{
        fontSize: 16,
        color: textColors["minTransWhite"]
    }
});