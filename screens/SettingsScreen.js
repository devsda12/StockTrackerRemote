import React, { Component } from "react";
import { Alert, FlatList, Text, ToastAndroid, View } from "react-native";
import { List } from "react-native-paper";

export class SettingsScreen extends Component{
    render(){
        return(
            <View>
                <List.Item title="Profile" description="Customize profile properties" left={props => <List.Icon {...props} icon="account" />}
                onPress={() => Platform.OS === 'android' ? ToastAndroid.show("Feature not yet implemented", ToastAndroid.SHORT) : Alert.alert("Feature not yet implemented")}/>
                <List.Item title="Database connection" description="Manage local and external databases" left={props => <List.Icon {...props} icon="database" />}
                onPress={() => Platform.OS === 'android' ? ToastAndroid.show("Feature not yet implemented", ToastAndroid.SHORT) : Alert.alert("Feature not yet implemented")}/>
            </View>
        );
    }
}