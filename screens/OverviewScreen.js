import React, { Component, useState } from "react";
import { View, Text, FlatList, StyleSheet } from "react-native";
import { Card, FAB, Paragraph, Searchbar, Title } from "react-native-paper";
import { openDatabase, retrieveAll, retrieveAllByName, retrieveSmallAll } from "../data/ContentDBHelper";
import { dummyData } from "../data/DummyData";
import { colors, globalStyles, textColors } from "../styles/StyleSheet";

export class OverviewScreen extends Component{
    state = {
        dataArray: []
    }

    retrieveData = async function(){
        const data = await retrieveAll();

        let tempArray = []
        for(let i = 0; i < data.rows.length; i++){
            tempArray.push(data.rows.item(i));
        }

        this.setState({dataArray: tempArray});
    }

    updateData = async function(query){
        const data = await retrieveAllByName(query);

        let tempArray = []
        for(let i = 0; i < data.rows.length; i++){
            tempArray.push(data.rows.item(i));
        }

        this.setState({dataArray: tempArray});
    }

    componentDidMount(){
        this.navigationListener = this.props.navigation.addListener('focus', () => {
            this.retrieveData();
        });
    }

    componentDidUpdate(){
        if(this.props.overViewSearchBarQuery !== ""){
            this.updateData(this.props.overViewSearchBarQuery);
            this.filteredData = true;
        }else if(this.filteredData){
            this.retrieveData();
            this.filteredData = false;
        }
        // console.log(this.props.overViewSearchBarQuery);
    }
    
    componentWillUnmount(){
        this.navigationListener();
    }

    render(){
        return(
            <View style={styles.listContainer}>
                {this.state.dataArray.length === 0 &&
                    <Text style={styles.noDataFoundText}>No data found...</Text>
                }

                {this.state.dataArray.length !== 0 &&
                    <FlatList style={styles.listComponent}
                    data={this.state.dataArray}
                    renderItem={({item}) => (
                        <ListItem item={item} navigation={this.props.navigation}/>
                    )}
                    numColumns={2}/>
                }
                
                <FAB
                style={styles.fab}
                icon="plus"
                onPress={() => this.props.navigation.navigate("AddItemScreen")}/>
            </View>
        );
    }
}

class ListItem extends Component{
    render(){
        return(
            <View style={styles.listItem}>
                <Card onPress={() => { 
                    this.props.navigation.navigate("AddItemScreen", {
                        id: this.props.item.id,
                        name: this.props.item.name,
                        description: this.props.item.description,
                        category: this.props.item.category,
                        condition: this.props.item.condition,
                        visual: this.props.item.visual
                    });
                }}>
                    {this.props.item.visual != null && //Weird way to do if statements within the JSX code
                        <Card.Cover source={{uri: this.props.item.visual}} />
                    }
                    <Card.Title title={this.props.item.name}/>
                    
                    {this.props.item.description != null && this.props.item.description != "" && 
                        <Card.Content>
                            <Paragraph>{this.props.item.description.length > 20 ? this.props.item.description.substring(0, 20) + "..." : this.props.item.description}</Paragraph>
                        </Card.Content>
                    }
                </Card>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    noDataFoundText:{
        top: 10,
        alignSelf: "center",
        color: textColors.disabledText
    },
    listContainer:{
        flex: 1,
    },
    listComponent:{
        // borderColor: "blue", borderWidth: 1,
    },
    listItem:{
        // borderColor: "red", borderWidth: 1,
        margin: 10,
        flex: 1
    },
    fab: {
        position: 'absolute',
        margin: 16,
        right: 0,
        bottom: 0,
        backgroundColor: colors.color_dark4_10
    }
});