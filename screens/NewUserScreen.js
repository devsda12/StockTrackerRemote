import React, { Component } from "react";
import { Text, View, Image, TouchableOpacity, Alert, ImageBackground, Platform, ToastAndroid } from "react-native";
import { insertUser, unameExists } from "../data/UserDBHelper";
import { textColors } from "../styles/StyleSheet";
import { landingScreenStyles, LoginTextField } from "./LandingScreen";


export default class NewUserScreen extends Component{
    state = {
        uname: "",
        passwd: ""
    }

    submit = function(){
        //Checking if uname / password is not null
        if(this.state.uname === "" || this.state.passwd === ""){
            if(Platform.OS === "android") ToastAndroid.show("No username / password specified!", ToastAndroid.SHORT);
            else Alert.alert("No username / password specified!");
            return;
        }

        //Checking if username exists
        unameExists(this.state.uname).then((result) => {
            if(result){
                if(Platform.OS === "android") ToastAndroid.show("Username already taken!", ToastAndroid.SHORT);
                else Alert.alert("Username already taken!");
                return;
            }

            //Adding it to the database
            insertUser(this.state.uname, this.state.passwd);
            if(Platform.OS === "android") ToastAndroid.show("Account created succesfully!", ToastAndroid.SHORT);
            // this.props.navigation.navigate("LandingScreen");
            this.props.navigation.goBack();
        })
        .catch((error) => {
            console.log(error);
            if(Platform.OS === "android") ToastAndroid.show("Unknown error while checking for username", ToastAndroid.SHORT);
            else Alert.alert("Unknown error while checking for username");
        });
    }

    render(){
        return(
            <ImageBackground source={require("../assets/landingScreenBackground.png")} style={landingScreenStyles.imageBackground}>
                <View style={landingScreenStyles.loginContainer}>
                    <Image style={landingScreenStyles.textLogo} source={require("../assets/logo_text.png")}/>
                    <LoginTextField placeholder="New Username" secureTextEntry={false} onValueChange={(value) => this.setState({uname: value}) }/>
                    <LoginTextField placeholder="New Password" secureTextEntry={true} onValueChange={(value) => this.setState({passwd: value}) }/>
                    <TouchableOpacity style={landingScreenStyles.loginButton} onPress={() => this.submit()}>
                        <Text style={landingScreenStyles.text}>Submit</Text>
                    </TouchableOpacity>
                    <Text style={{color: textColors["minTransWhite"], fontSize: 12, marginTop: 15}}>*Only local databases supported at the moment</Text>
                </View>
            </ImageBackground>
        );
    }
}