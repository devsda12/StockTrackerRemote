import { Picker } from '@react-native-picker/picker';
import React, { Component } from 'react';
import { Image, Platform, StyleSheet, ToastAndroid, View } from 'react-native';
import { ActivityIndicator, Button, FAB, TextInput } from 'react-native-paper';
import { deleteItem, insertItem, updateItem } from '../data/ContentDBHelper';
import { borderColors, colors, textColors } from '../styles/StyleSheet';
import * as FileSystem from 'expo-file-system';

export class AddItemScreen extends Component{
    state = {
        id: this.props.route.params !== undefined && this.props.route.params.hasOwnProperty('id') ? this.props.route.params.id : null,
        name: this.props.route.params !== undefined && this.props.route.params.hasOwnProperty('name') ? this.props.route.params.name : null,
        description: this.props.route.params !== undefined && this.props.route.params.hasOwnProperty('description') ? this.props.route.params.description : null,
        category: this.props.route.params !== undefined && this.props.route.params.hasOwnProperty('category') ? this.props.route.params.category : null,
        condition: this.props.route.params !== undefined && this.props.route.params.hasOwnProperty('condition') ? this.props.route.params.condition : "unknown",
        visual: this.props.route.params !== undefined && this.props.route.params.hasOwnProperty('visual') ? this.props.route.params.visual : null,

        customVisual: false,
        loadingAddition: false
    }

    setNameState = (newName) => this.setState({name: newName});
    setDescriptionState = (newDesc) => this.setState({description: newDesc});
    setCategoryState = (newCat) => this.setState({category: newCat});
    setConditionState = (newCond) => this.setState({condition: newCond});
    setVisualState = (newVisual) => this.setState({visual: newVisual});

    componentDidMount(){
        this.navigationListener = this.props.navigation.addListener('focus', () => {
            if(this.props.route.params?.takenPictureURI){
                this.setState({visual: this.props.route.params.takenPictureURI});
            }
        });
    }

    componentWillUnmount(){
        this.navigationListener();
    }

    relocateVisual = async () => {
        let dirPath = FileSystem.documentDirectory + "images-" + global.currUID + "/";
        let dirInfo = await FileSystem.getInfoAsync(dirPath);
        if(!dirInfo.exists){
            try{
                await FileSystem.makeDirectoryAsync(dirPath);
            } catch{console.log("Tried to create a directory which already exists");}
        }

        let read = await FileSystem.readDirectoryAsync(dirPath);
        console.log(read);

        let filePath = dirPath + this.state.id + ".jpg";
        let fileInfo = await FileSystem.getInfoAsync(filePath);
        if(fileInfo.exists){
            try{
                await FileSystem.deleteAsync(filePath);
            } catch(e){console.log(e)}
        }

        //Moving it
        try{
            await FileSystem.moveAsync({from: this.state.visual, to: filePath});
        } catch(e){console.log(e)}
    }

    saveContent = async () => {
        //Check the entries
        if(this.state.name === null || this.state.name === ""){
            Platform.OS === 'android' ? ToastAndroid.show("Please do not leave the name empty!", ToastAndroid.SHORT) : Alert.alert("Please do not leave the name empty!");
            return;
        }

        this.setState({loadingAddition: true});

        if(this.state.id === null){ //This is a new entry
            insertItem(this.state.name, this.state.description, this.state.category, this.state.condition, this.state.visual)
            .then(async ({uid, result}) =>{
                this.state.id = uid;
                
                //Relocating the visual - DISABLED FOR NOW AS NOT MOVING IT SEEMS TO BE WORKING BETTER
                //if(this.state.visual !== null) await this.relocateVisual();
                
                Platform.OS === 'android' ? ToastAndroid.show("Entry added to the database", ToastAndroid.SHORT) : _;
                this.setState({loadingAddition: false})
                this.props.navigation.goBack();
            })
            .catch((error) => {
                console.log(error);
                this.setState({loadingAddition: false})
                Platform.OS === 'android' ? ToastAndroid.show("Error while adding entry", ToastAndroid.SHORT) : Alert.alert("Error while adding entry");
            });
        }else{
            updateItem(this.state.id, {name: this.state.name, description: this.state.description, category: this.state.category, condition: this.state.condition, visual: this.state.visual})
            .then(async (result) => {
                //Relocating the visual - DISABLED FOR NOW AS NOT MOVING IT SEEMS TO BE WORKING BETTER
                //if(this.state.visual !== null) await this.relocateVisual();
                
                Platform.OS === 'android' ? ToastAndroid.show("Entry updated in the database", ToastAndroid.SHORT) : _;
                this.setState({loadingAddition: false})
                this.props.navigation.goBack();
            })
            .catch((error) => {
                console.log(error);
                this.setState({loadingAddition: false})
                Platform.OS === 'android' ? ToastAndroid.show("Error while updating entry", ToastAndroid.SHORT) : Alert.alert("Error while updating entry");
            });
        }
    }

    removeContent = async () => {
        deleteItem(this.state.id)
        .then((result) => {
            Platform.OS === 'android' ? ToastAndroid.show("Succesfully removed entry", ToastAndroid.SHORT) : Alert.alert("Succesfully removed entry");
            this.props.navigation.goBack();
        })
        .catch((error) => {
            console.log(error);
            Platform.OS === 'android' ? ToastAndroid.show("Error while removing entry", ToastAndroid.SHORT) : Alert.alert("Error while removing entry");
        });
    }

    render(){
        return(
            <View style={{flex: 1}}>
                <View style={styles.visualContainer}>
                    <Image style={styles.visual} source={{uri: this.state.visual}}/>
                    {this.state.visual === null && 
                        <View style={{width: "100%", padding: 50}}>
                            <Image style={styles.noImageVisual} source={require("../assets/no-image.png")} />
                        </View>
                    }
                    <FAB style={[styles.imageFAB, {marginRight: 16, right: 0}]} icon="image-outline" onPress={() => console.log(this.state.condition)}/>
                    <FAB style={[styles.imageFAB, {marginRight: 90, right: 0}]} icon="camera" onPress={() => this.props.navigation.navigate("CameraScreen")}/>
                </View>
                <ContentView 
                setName={this.setNameState} 
                setDesc={this.setDescriptionState} 
                setCat={this.setCategoryState} 
                setCond={this.setConditionState}
                loadingAddition={this.state.loadingAddition}
                saveButtonOnPress={this.saveContent}
                cancelButtonOnPress={() => this.props.navigation.goBack()}
                removeButtonOnPress={() => this.removeContent()}
                isNewEntry={this.state.id === null}
                existingInfo={
                    this.state.id !== null ? {name: this.state.name, description: this.state.description, condition: this.state.condition} : null
                }/>
            </View>
        );
    }
}

class ContentView extends Component{
    render(){
        return(
            <View style={styles.contentView}>
                <TextInput 
                style={[styles.contentElement, {marginTop: 30}]} 
                label="Name" 
                mode="outlined" 
                defaultValue={this.props.existingInfo !== null && this.props.existingInfo.name !== null ? this.props.existingInfo.name : ""} 
                onChangeText={(text) => this.props.setName(text)}/>
                <TextInput 
                style={styles.contentElement} 
                label="Description" 
                mode="outlined" 
                multiline={true} 
                numberOfLines={3} 
                defaultValue={this.props.existingInfo !== null && this.props.existingInfo.description !== null ? this.props.existingInfo.description : ""} 
                onChangeText={(text) => this.props.setDesc(text)}/>
                
                {/*TODO: Make category choices*/}
                
                <View style={[styles.contentElement, styles.contentPicker]}>
                    <Picker selectedValue={this.props.existingInfo !== null && this.props.existingInfo.condition !== null ? this.props.existingInfo.condition : "unknown"} onValueChange={(itemValue, itemIndex) => this.props.setCond(itemValue)}>
                        <Picker.Item label="Unknown" value="unknown"/>
                        <Picker.Item label="Broken" value="broken"/>
                        <Picker.Item label="Defects" value="defects"/>
                        <Picker.Item label="Partial" value="partial"/>
                        <Picker.Item label="Operable" value="operable"/>
                        <Picker.Item label="As new" value="as new"/>
                    </Picker>
                </View>

                {!this.props.loadingAddition && 
                    <Button style={styles.saveButton} icon="content-save" mode="contained" onPress={() => this.props.saveButtonOnPress()}>Save</Button>
                }
                {this.props.loadingAddition && 
                    <View style={styles.loginActivityIndicatorContainer}><ActivityIndicator animating={true} /></View>
                }

                {this.props.isNewEntry && 
                    <Button style={styles.cancelButton} icon="delete" color={borderColors.lightGrey} onPress={() => this.props.cancelButtonOnPress()}>Cancel</Button>
                }
                {!this.props.isNewEntry && 
                    <Button style={styles.cancelButton} icon="delete" color={borderColors.lightGrey} onPress={() => this.props.removeButtonOnPress()}>Remove entry</Button>
                }
                
            </View>
        );
    }
}

const styles = StyleSheet.create({
    visualContainer: {
        // borderColor: "green", borderWidth: 1,
        flex: 1,
        alignItems: 'center'
    },
    noImageVisual:{
        // borderColor: "blue", borderWidth: 1,
        resizeMode: 'contain',
        width: "100%", height: "100%"
    },
    visual:{
        // borderColor: "blue", borderWidth: 1,
        resizeMode: 'contain',
        width: "100%", height: "100%",
        position: "absolute", 
        backgroundColor: colors.color_dark3_10
    },
    imageFAB:{
        position: 'absolute',
        bottom: -25,
        backgroundColor: colors.color_dark5_10
    },
    contentView:{
        // borderColor: "red", borderWidth: 1,
        flex: 2
    },
    contentElement:{
        marginTop: 15,
        marginHorizontal: 30
    },
    contentPicker:{
        height: 50, 
        borderWidth: 1, 
        borderColor: borderColors.lightGrey, 
        borderRadius: 4,
        justifyContent: 'center'
    },
    saveButton: {
        position: "absolute",
        bottom: 10,
        right: 10,
        backgroundColor: colors.color_dark3_10
    },
    cancelButton: {
        position: "absolute",
        bottom: 10,
        left: 10,
    },
    loginActivityIndicatorContainer: {
        position: "absolute",
        bottom: 20,
        right: 100
    }
});