import { Camera } from "expo-camera";
import React, { Component } from "react";
import { Dimensions, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { FAB } from "react-native-paper";
import { colors } from "../styles/StyleSheet";

//#region Consts
const { height, width } = Dimensions.get('window');
const screenRatio = height / width;
//#endregion

export class CameraScreen extends Component{
    state = {
        hasPermission: false,
        type: Camera.Constants.Type.back,
        
        ratioIsSet: false,
        ratio: "4:3",
        imagePadding: 0
    }

    async componentDidMount(){
        const { status } = await Camera.requestPermissionsAsync();
        this.setState({hasPermission: status === 'granted'});
    }

    setCameraReady = async () => {
        if (!this.state.ratioIsSet) {
          await this.prepareRatio();
        }
    };

    prepareRatio = async function() {
        let desiredRatio = '4:3';  // Start with the system default
        // This issue only affects Android
        if (Platform.OS === 'android') {
            const ratios = await this.camera.getSupportedRatiosAsync();

            // Calculate the width/height of each of the supported camera ratios
            // These width/height are measured in landscape mode
            // find the ratio that is closest to the screen ratio without going over
            let distances = {};
            let realRatios = {};
            let minDistance = null;
            for (const ratio of ratios) {
                const parts = ratio.split(':');
                const realRatio = parseInt(parts[0]) / parseInt(parts[1]);
                realRatios[ratio] = realRatio;
                // ratio can't be taller than screen, so we don't want an abs()
                const distance = screenRatio - realRatio; 
                distances[ratio] = realRatio;
                if (minDistance == null) {
                    minDistance = ratio;
                } else {
                    if (distance >= 0 && distance < distances[minDistance]) {
                        minDistance = ratio;
                    }
                }
            }
            // set the best match
            desiredRatio = minDistance;
            //  calculate the difference between the camera width and the screen height
            const remainder = Math.floor(
                (height - realRatios[desiredRatio] * width) / 2
            );
            // set the preview padding and preview ratio
            this.setState({imagePadding: remainder});
            this.setState({ratio: desiredRatio});
            // Set a flag so we don't do this 
            // calculation each time the screen refreshes
            this.setState({ratioIsSet: true});
        }
    }

    takePicture = async () => {
        if(this.camera){
            let photo = await this.camera.takePictureAsync();
            
            //Returning the temporary path of the picture - THIS SHOULD BE DYNAMIC, BUT GOING STRAIGHT BACK TO ADDITEMSCREEN FOR NOW
            this.props.navigation.navigate({name: "AddItemScreen", params: {takenPictureURI: photo.uri}, merge: true});
        }
    }

    render(){
        if (this.state.hasPermission === null) {
            return <View />;
        }
        if (this.state.hasPermission === false) {
            return <Text>No access to camera</Text>;
        }

        return(
            <View style={styles.cameraContainer}>
                <Camera style={[styles.camera]} 
                type={this.state.type} 
                onCameraReady={this.setCameraReady}
                ref={(ref) => this.camera = ref}
                ratio={this.state.ratio}>
                    <View style={styles.buttonsContainer}>
                        <View style={styles.buttonContainer}><FAB style={styles.fab} small icon="arrow-left" onPress={() => this.props.navigation.goBack()}/></View>
                        <View style={styles.buttonContainer}><FAB style={styles.fab} icon="camera-iris" onPress={() => this.takePicture()}/></View>
                        <View style={styles.buttonContainer}>
                            <FAB style={styles.fab} small icon="rotate-left" 
                            onPress={() => this.setState({type: this.state.type === Camera.Constants.Type.back ? Camera.Constants.Type.front : Camera.Constants.Type.back})}/>
                        </View>
                        {/* <TouchableOpacity
                        style={styles.button}
                        onPress={() => {
                            this.setState({type: 
                                this.state.type === Camera.Constants.Type.back
                                    ? Camera.Constants.Type.front
                                    : Camera.Constants.Type.back
                            });
                        }}>
                            <Text style={styles.text}> Flip </Text>
                        </TouchableOpacity> */}
                    </View>
                </Camera>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    cameraContainer:{
        flex: 1
    },
    camera:{
        width: "100%", height: "100%"
    },
    buttonsContainer:{
        position: "absolute",
        bottom: 15,
        flexDirection: "row"
    },
    buttonContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: "center"
    },
    fab: {
        backgroundColor: colors.color_dark5_10
    }
});