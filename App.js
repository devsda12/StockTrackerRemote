import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import React, {Component, useState} from 'react';
import { StatusBar, StyleSheet, Text, View } from 'react-native';
import { LandingScreen } from './screens/LandingScreen.js'
import { OverviewScreen } from './screens/OverviewScreen.js';
import { colors, globalStyles } from './styles/StyleSheet.js';
import { SettingsScreen } from './screens/SettingsScreen.js';
import NewUserScreen from './screens/NewUserScreen.js';
import './data/GlobalVars.js'
import { AddItemScreen } from './screens/AddItemScreen.js';
import { CameraScreen } from './screens/CameraScreen.js';
import { Ionicons } from '@expo/vector-icons';
import { Appbar, Searchbar } from 'react-native-paper';

//#region Consts
const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();
//#endregion

export default class App extends Component {
  render(){
    return (
      <NavigationContainer>
        <StatusBar backgroundColor={colors["color_dark4_10"]} barStyle="light-content"/>
        <Stack.Navigator>
          <Stack.Screen name="LandingScreen" component={LandingScreen} options={{headerShown: false}}/>
          <Stack.Screen name="NewUserScreen" component={NewUserScreen} options={{ title: "Create Account", headerStyle: globalStyles.dark3_10, headerTintColor: "white"}}/>
          <Stack.Screen name="AppContent" component={AppContent} options={{headerShown: false}}/>
          <Stack.Screen name="AddItemScreen" component={AddItemScreen} options={{headerShown: false}}/>
          <Stack.Screen name="CameraScreen" component={CameraScreen} options={{headerShown: false}}/>
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

class AppContent extends Component{
  state = {
    overViewSearchBarQuery: ""
  }

  onOverviewSearchBarChange = (newQuery) => {this.setState({overViewSearchBarQuery: newQuery})};

  render(){
    return(
      <Tab.Navigator 
      initialRouteName="OverViewScreen"
      screenOptions={({route}) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;

          if (route.name === 'Overview') {
            iconName = focused ? 'home-sharp' : 'home-outline';
          } else if (route.name === 'Settings') {
            iconName = focused ? 'settings-sharp' : 'settings-outline';
          }

          // You can return any component that you like here!
          return <Ionicons name={iconName} size={size} color={color} />;
        }, tabBarActiveTintColor: colors.color_dark4_10
      })}>
        <Tab.Screen 
          name="Overview"
          options={{
            header: () => <ScreenHeader searchBarChangeCallback={this.onOverviewSearchBarChange}/>,
            headerTintColor: "white"
          }}
        >
          {(props) => <OverviewScreen {...props} overViewSearchBarQuery={this.state.overViewSearchBarQuery}/>}
        </Tab.Screen>
        <Tab.Screen 
          name="Settings" 
          component={SettingsScreen}
          options={{
            headerStyle: globalStyles.dark3_10,
            headerTintColor: "white"
          }}
        />
      </Tab.Navigator>
    );
  }
}

function ScreenHeader(props){
  const [searchBarExpanded, setSearchBarExpanded] = useState(false);
  const [searchQuery, setSearchQuery] = useState("");

  return(
    <Appbar.Header style={styles.screenHeader}>
      {!searchBarExpanded &&
        <Appbar.Content title="Overview"/>
      }
      
      {searchBarExpanded && 
        <View style={{flex: 1, flexDirection: "row"}}>
          <Appbar.BackAction color="white" onPress={() => setSearchBarExpanded(false)} />
          <Searchbar placeholder="Search" onChangeText={(text) => {setSearchQuery(text); props.searchBarChangeCallback(text)}} value={searchQuery}/>
        </View>
      }

      {!searchBarExpanded &&
        <Appbar.Action icon="magnify" onPress={() => {setSearchBarExpanded(true)}} />
      }
    </Appbar.Header>
  );
}

const styles = StyleSheet.create({
  screenHeader: {
    backgroundColor: colors.color_dark3_10
  }
});